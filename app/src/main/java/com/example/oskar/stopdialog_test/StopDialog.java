package com.example.oskar.stopdialog_test;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

/**
 * Created by oskar on 20.12.2016.
 */

public class StopDialog extends DialogFragment{


    //DialogBuilder
    @Override
    public AlertDialog onCreateDialog(Bundle SavedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder .setView(inflater.inflate(R.layout.stop_dialog_layout,null))
                .setMessage(R.string.frage)
                .setPositiveButton(R.string.ja, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){}})
                .setNegativeButton(R.string.nein, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){

                    }
                });


        return builder.create();

    }



}
