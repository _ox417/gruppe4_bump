package com.example.oskar.stopdialog_test;


import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                confirmStop();
            }
        });
    }





        public void confirmStop() {
        FragmentManager manager = getFragmentManager();
        StopDialog stopDialog = new StopDialog();
        stopDialog.show(manager,"confirmStop");

        }
}

